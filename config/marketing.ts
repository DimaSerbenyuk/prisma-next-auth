import { MarketingConfig } from "types"

export const marketingConfig: MarketingConfig = {
  mainNav: [
    {
      title: "Main",
      href: "/",
    },
    {
      title: "Classes",
      href: "/pricing",
    },
    {
      title: "Calendar",
      href: "/blog",
    },
    {
      title: "Articles",
      href: "/docs",
    },
    {
      title: "Videos",
      href: "/contact",
    },
    {
      title: "Ministries",
      href: "/contact",
    },
    {
      title: "Stuff",
      href: "/stuff",
    },
    {
      title: "Contact",
      href: "/contact",
      disabled: true,
    },
  ],
}
